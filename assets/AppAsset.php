<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'custom/assets/vendors/base/vendors.bundle.css',
        'custom/assets/demo/demo9/base/style.bundle.css',
        'custom/assets/vendors/custom/fullcalendar/fullcalendar.bundle.css',
    ];
    public $js = [
        'custom/assets/vendors/base/vendors.bundle.js',
        'custom/assets/demo/demo9/base/scripts.bundle.js',
        'custom/assets/vendors/custom/fullcalendar/fullcalendar.bundle.js',
        'custom/assets/app/js/dashboard.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
