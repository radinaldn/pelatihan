<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Penjamin;

/**
 * PenjaminSearch represents the model behind the search form of `app\models\Penjamin`.
 */
class PenjaminSearch extends Penjamin
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['penjamin_id'], 'integer'],
            [['nama_penjamin', 'tipe_penjamin', 'alamat_penjamin', 'no_telp_penjamin', 'email_penjamin'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Penjamin::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'penjamin_id' => $this->penjamin_id,
        ]);

        $query->andFilterWhere(['ilike', 'nama_penjamin', $this->nama_penjamin])
            ->andFilterWhere(['ilike', 'tipe_penjamin', $this->tipe_penjamin])
            ->andFilterWhere(['ilike', 'alamat_penjamin', $this->alamat_penjamin])
            ->andFilterWhere(['ilike', 'no_telp_penjamin', $this->no_telp_penjamin])
            ->andFilterWhere(['ilike', 'email_penjamin', $this->email_penjamin]);

        return $dataProvider;
    }
}
