<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "kelurahan".
 *
 * @property int $kel_id
 * @property string $kelurahan
 * @property int $kec_id
 * @property int $kab_id
 * @property string $kode_pos
 * @property int $prov_id
 *
 * @property Kabupaten $kab
 * @property Kecamatan $kec
 * @property Provinsi $prov
 */
class Kelurahan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'kelurahan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kel_id'], 'required'],
            [['kel_id', 'kec_id', 'kab_id', 'prov_id'], 'default', 'value' => null],
            [['kel_id', 'kec_id', 'kab_id', 'prov_id'], 'integer'],
            [['kelurahan'], 'string', 'max' => 150],
            [['kode_pos'], 'string', 'max' => 8],
            [['kel_id'], 'unique'],
            [['kab_id'], 'exist', 'skipOnError' => true, 'targetClass' => Kabupaten::className(), 'targetAttribute' => ['kab_id' => 'kab_id']],
            [['kec_id'], 'exist', 'skipOnError' => true, 'targetClass' => Kecamatan::className(), 'targetAttribute' => ['kec_id' => 'kec_id']],
            [['prov_id'], 'exist', 'skipOnError' => true, 'targetClass' => Provinsi::className(), 'targetAttribute' => ['prov_id' => 'prov_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kel_id' => 'Kel ID',
            'kelurahan' => 'Kelurahan',
            'kec_id' => 'Kec ID',
            'kab_id' => 'Kab ID',
            'kode_pos' => 'Kode Pos',
            'prov_id' => 'Prov ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKab()
    {
        return $this->hasOne(Kabupaten::className(), ['kab_id' => 'kab_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKec()
    {
        return $this->hasOne(Kecamatan::className(), ['kec_id' => 'kec_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProv()
    {
        return $this->hasOne(Provinsi::className(), ['prov_id' => 'prov_id']);
    }
}
