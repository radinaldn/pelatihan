<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ppk".
 *
 * @property int $ppk_id
 * @property int $kode_ppk
 * @property string $nama_ppk
 * @property string $alamat_ppk
 */
class Ppk extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ppk';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ppk_id'], 'required'],
            [['ppk_id', 'kode_ppk'], 'default', 'value' => null],
            [['ppk_id', 'kode_ppk'], 'integer'],
            [['alamat_ppk'], 'string'],
            [['nama_ppk'], 'string', 'max' => 75],
            [['ppk_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ppk_id' => 'Ppk ID',
            'kode_ppk' => 'Kode Ppk',
            'nama_ppk' => 'Nama Ppk',
            'alamat_ppk' => 'Alamat Ppk',
        ];
    }
}
