<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "kabupaten".
 *
 * @property int $kab_id
 * @property string $kabupaten
 * @property int $orderno
 * @property int $prov_id
 *
 * @property Provinsi $prov
 * @property Kecamatan[] $kecamatans
 * @property Kelurahan[] $kelurahans
 */
class Kabupaten extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'kabupaten';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kab_id'], 'required'],
            [['kab_id', 'orderno', 'prov_id'], 'default', 'value' => null],
            [['kab_id', 'orderno', 'prov_id'], 'integer'],
            [['kabupaten'], 'string', 'max' => 100],
            [['kab_id'], 'unique'],
            [['prov_id'], 'exist', 'skipOnError' => true, 'targetClass' => Provinsi::className(), 'targetAttribute' => ['prov_id' => 'prov_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kab_id' => 'Kab ID',
            'kabupaten' => 'Kabupaten',
            'orderno' => 'Orderno',
            'prov_id' => 'Prov ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProv()
    {
        return $this->hasOne(Provinsi::className(), ['prov_id' => 'prov_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKecamatans()
    {
        return $this->hasMany(Kecamatan::className(), ['kab_id' => 'kab_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKelurahans()
    {
        return $this->hasMany(Kelurahan::className(), ['kab_id' => 'kab_id']);
    }
}
