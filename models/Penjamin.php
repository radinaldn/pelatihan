<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "penjamin".
 *
 * @property int $penjamin_id
 * @property string $nama_penjamin
 * @property string $tipe_penjamin
 * @property string $alamat_penjamin
 * @property string $no_telp_penjamin
 * @property string $email_penjamin
 */
class Penjamin extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'penjamin';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['penjamin_id'], 'required'],
            [['penjamin_id'], 'default', 'value' => null],
            [['penjamin_id'], 'integer'],
            [['nama_penjamin', 'tipe_penjamin', 'alamat_penjamin', 'no_telp_penjamin', 'email_penjamin'], 'string', 'max' => 255],
            [['penjamin_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'penjamin_id' => 'Penjamin ID',
            'nama_penjamin' => 'Nama Penjamin',
            'tipe_penjamin' => 'Tipe Penjamin',
            'alamat_penjamin' => 'Alamat Penjamin',
            'no_telp_penjamin' => 'No Telp Penjamin',
            'email_penjamin' => 'Email Penjamin',
        ];
    }
}
