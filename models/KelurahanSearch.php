<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Kelurahan;

/**
 * KelurahanSearch represents the model behind the search form of `app\models\Kelurahan`.
 */
class KelurahanSearch extends Kelurahan
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kel_id', 'kec_id', 'kab_id', 'prov_id'], 'integer'],
            [['kelurahan', 'kode_pos'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Kelurahan::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'kel_id' => $this->kel_id,
            'kec_id' => $this->kec_id,
            'kab_id' => $this->kab_id,
            'prov_id' => $this->prov_id,
        ]);

        $query->andFilterWhere(['ilike', 'kelurahan', $this->kelurahan])
            ->andFilterWhere(['ilike', 'kode_pos', $this->kode_pos]);

        return $dataProvider;
    }
}
