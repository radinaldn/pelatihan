<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Ppk;

/**
 * PpkSearch represents the model behind the search form of `app\models\Ppk`.
 */
class PpkSearch extends Ppk
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ppk_id', 'kode_ppk'], 'integer'],
            [['nama_ppk', 'alamat_ppk'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Ppk::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ppk_id' => $this->ppk_id,
            'kode_ppk' => $this->kode_ppk,
        ]);

        $query->andFilterWhere(['ilike', 'nama_ppk', $this->nama_ppk])
            ->andFilterWhere(['ilike', 'alamat_ppk', $this->alamat_ppk]);

        return $dataProvider;
    }
}
