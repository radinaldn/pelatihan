<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "kecamatan".
 *
 * @property int $kec_id
 * @property string $kecamatan
 * @property int $kab_id
 * @property int $prov_id
 *
 * @property Kabupaten $kab
 * @property Provinsi $prov
 * @property Kelurahan[] $kelurahans
 */
class Kecamatan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'kecamatan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kec_id', 'kab_id'], 'required'],
            [['kec_id', 'kab_id', 'prov_id'], 'default', 'value' => null],
            [['kec_id', 'kab_id', 'prov_id'], 'integer'],
            [['kecamatan'], 'string', 'max' => 100],
            [['kec_id'], 'unique'],
            [['kab_id'], 'exist', 'skipOnError' => true, 'targetClass' => Kabupaten::className(), 'targetAttribute' => ['kab_id' => 'kab_id']],
            [['prov_id'], 'exist', 'skipOnError' => true, 'targetClass' => Provinsi::className(), 'targetAttribute' => ['prov_id' => 'prov_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kec_id' => 'Kec ID',
            'kecamatan' => 'Kecamatan',
            'kab_id' => 'Kab ID',
            'prov_id' => 'Prov ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKab()
    {
        return $this->hasOne(Kabupaten::className(), ['kab_id' => 'kab_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProv()
    {
        return $this->hasOne(Provinsi::className(), ['prov_id' => 'prov_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKelurahans()
    {
        return $this->hasMany(Kelurahan::className(), ['kec_id' => 'kec_id']);
    }
}
