<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PenjaminSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Penjamins';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="penjamin-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Penjamin', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'penjamin_id',
            'nama_penjamin',
            'tipe_penjamin',
            'alamat_penjamin',
            'no_telp_penjamin',
            //'email_penjamin:email',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
