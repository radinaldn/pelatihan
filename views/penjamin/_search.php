<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PenjaminSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="penjamin-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'penjamin_id') ?>

    <?= $form->field($model, 'nama_penjamin') ?>

    <?= $form->field($model, 'tipe_penjamin') ?>

    <?= $form->field($model, 'alamat_penjamin') ?>

    <?= $form->field($model, 'no_telp_penjamin') ?>

    <?php // echo $form->field($model, 'email_penjamin') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
