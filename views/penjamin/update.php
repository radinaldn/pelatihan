<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Penjamin */

$this->title = 'Update Penjamin: ' . $model->penjamin_id;
$this->params['breadcrumbs'][] = ['label' => 'Penjamins', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->penjamin_id, 'url' => ['view', 'id' => $model->penjamin_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="penjamin-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
