<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Penjamin */

$this->title = 'Create Penjamin';
$this->params['breadcrumbs'][] = ['label' => 'Penjamins', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="penjamin-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
