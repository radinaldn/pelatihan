<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Ppk */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ppk-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ppk_id')->textInput() ?>

    <?= $form->field($model, 'kode_ppk')->textInput() ?>

    <?= $form->field($model, 'nama_ppk')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'alamat_ppk')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
