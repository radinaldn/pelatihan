<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Ppk */

$this->title = 'Create Ppk';
$this->params['breadcrumbs'][] = ['label' => 'Ppks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ppk-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
