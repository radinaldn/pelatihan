<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Ppk */

$this->title = 'Update Ppk: ' . $model->ppk_id;
$this->params['breadcrumbs'][] = ['label' => 'Ppks', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ppk_id, 'url' => ['view', 'id' => $model->ppk_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ppk-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
