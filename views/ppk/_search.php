<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PpkSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ppk-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ppk_id') ?>

    <?= $form->field($model, 'kode_ppk') ?>

    <?= $form->field($model, 'nama_ppk') ?>

    <?= $form->field($model, 'alamat_ppk') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
